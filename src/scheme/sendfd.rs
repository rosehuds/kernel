use alloc::{
    collections::{btree_map::Entry, BTreeMap},
    string::String,
};
use syscall::{
    data::ReceivedFd,
    error::*,
    flag::{EVENT_READ, O_CREAT, O_NONBLOCK},
    scheme::Scheme,
};
use spin::RwLock;
use core::sync::atomic::{AtomicUsize, Ordering};

use crate::{
    context::{ContextId, context_id, contexts_mut},
    scheme::{FileHandle, SchemeId},
    sync::WaitQueue,
};

enum Connection {
    Listener {
        entry: String,
        blocking: bool,
    },
    Sender {
        other_end: String,
    },
}

struct Listener {
    pub context_id: ContextId,
    pub read_queue: WaitQueue<ReceivedFd>,
}

pub struct SendFdScheme {
    scheme_id: SchemeId,
    next_id: AtomicUsize,
    handles: RwLock<BTreeMap<usize, Connection>>,
    listeners: RwLock<BTreeMap<String, Listener>>,
}

impl SendFdScheme {
    pub fn new(scheme_id: SchemeId) -> Self {
        SendFdScheme {
            scheme_id,
            next_id: AtomicUsize::new(0),
            handles: RwLock::new(BTreeMap::new()),
            listeners: RwLock::new(BTreeMap::new()),
        }
    }
}

impl Scheme for SendFdScheme {
    fn open(&self, path: &[u8], flags: usize, _uid: u32, _gid: u32) -> Result<usize> {
        let path = core::str::from_utf8(path).map_err(|_| Error::new(EINVAL))?;
        match flags & O_CREAT {
            O_CREAT => {
                let id = self.next_id.fetch_add(1, Ordering::SeqCst);
                let connection = Connection::Listener {
                    entry: String::from(path),
                    blocking: flags & O_NONBLOCK == O_NONBLOCK,
                };
                self.handles.write().insert(id, connection);
                match self.listeners.write().entry(String::from(path)) {
                    Entry::Occupied(_) => return Err(Error::new(EEXIST)),
                    Entry::Vacant(entry) => entry.insert(Listener {
                        context_id: context_id(),
                        read_queue: WaitQueue::new(),
                    }),
                };
                Ok(id)
            },
            0 => {
                let listeners = self.listeners.read();
                listeners.get(path).ok_or(Error::new(ENOENT))?;
                let id = self.next_id.fetch_add(1, Ordering::SeqCst);
                let connection = Connection::Sender {
                    other_end: String::from(path),
                };
                self.handles.write().insert(id, connection);
                Ok(id)
            },
            _ => unreachable!(),
        }
    }

    fn read(&self, id: usize, buf: &mut [u8]) -> Result<usize> {
        let handles = self.handles.read();
        let connection = handles.get(&id).ok_or(Error::new(EBADF))?;
        match connection {
            Connection::Sender { .. } => return Err(Error::new(EINVAL)),
            Connection::Listener { entry, blocking } => {
                let listeners = self.listeners.read();
                let listener = listeners.get(entry).ok_or(Error::new(EBADF))?;

                let mut buf = unsafe {
                    core::slice::from_raw_parts_mut(
                        buf.as_mut_ptr() as *mut ReceivedFd,
                        buf.len() / core::mem::size_of::<ReceivedFd>()
                    )
                };
                let count = listener.read_queue.receive_into(&mut buf, *blocking)
                    .ok_or(Error::new(EINTR))?;

                Ok(count * core::mem::size_of::<ReceivedFd>())
            }
        }

    }

    fn write(&self, id: usize, buf: &[u8]) -> Result<usize> {
        let handles = self.handles.read();
        let connection = handles.get(&id).ok_or(Error::new(EBADF))?;
        match connection {
            Connection::Listener { .. } => return Err(Error::new(EINVAL)),
            Connection::Sender { other_end } => {
                let listeners = self.listeners.write();
                let mut listener = listeners.get_mut(other_end).ok_or(Error::new(EBADF))?;

                let mut buf = unsafe {
                    core::slice::from_raw_parts(
                        buf.as_ptr() as *const usize,
                        buf.len() / core::mem::size_of::<usize>(),
                    )
                };
                let fd_count = buf.len();
                let contexts = contexts_mut();
                let current_ctx_id = context_id();
                let current = contexts.get(current_ctx_id).ok_or(Error::new(ESRCH))?.write();
                let target = contexts.get(listener.context_id).ok_or(Error::new(ESRCH))?.write();
                for &fd in buf {
                    let desc = current.remove_file(FileHandle::from(fd))
                        .ok_or(Error::new(ENOENT))?;
                    // if a listener is being sent, update listener entry's context id
                    {
                        let description = desc.description.read();
                        if description.scheme == self.scheme_id {
                            let connection = self.handles.read().get(&description.number)
                                .ok_or(Error::new(EBADF))?;
                            if let Connection::Listener { entry, .. } = connection {
                                let mut listeners = self.listeners.write();
                                // set the fd's associated listener's context id
                                // to the target of this send op
                                listeners.get_mut(entry)
                                    .ok_or(Error::new(EBADF))?.context_id = listener.context_id;
                            }
                        }
                    }
                    let target_fd = target.add_file(desc).ok_or(Error::new(EMFILE))?;
                    listener.read_queue.send(ReceivedFd {
                        fd: target_fd.into(),
                        from_pid: current_ctx_id.into(),
                    });
                }
                crate::event::trigger(self.scheme_id, id, EVENT_READ);
            }
        }
        Ok(0)
    }

    fn fevent(&self, id: usize, flags: usize) -> Result<usize> {
        if flags != EVENT_READ {
            return Err(Error::new(EINVAL));
        }
        if !self.handles.read().contains_key(&id) {
            return Err(Error::new(EBADF));
        }
        Ok(id)
    }

    fn close(&self, id: usize) -> Result<usize> {
        let mut handles = self.handles.write();
        let connection = handles.remove(&id).ok_or(Error::new(EBADF))?;
        match connection {
            Connection::Listener { entry, .. } => {
                self.listeners.write().remove(&entry).ok_or(Error::new(EBADF))?;
            },
            _ => {},
        };
        Ok(0)
    }
}
